import java.util.Scanner;
public class Shop{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		Clothes[] clothing = new Clothes[4];
		for (int i = 0; i < clothing.length ; i++){// loop to store object values into the array
			clothing[i] = new Clothes();
			System.out.println("What piece of clothing for #" + (i+1) + " are you looking for?: shirt, dress, pants, etc");
			clothing[i].type = sc.next(); //taking input for type of clothing 
			
			System.out.println("Enter the size: ");
			clothing[i].size = sc.next(); //taking input of size
			
			System.out.println("Enter the price");
			clothing[i].price = sc.nextDouble();
			System.out.println(""); // taking input of price
			
		}
		//receipt portion
		System.out.println("Receipt of your last item:");
		System.out.println("Your item: "+ clothing[3].type);
		System.out.println("Size you have chosen: "+ clothing[3].size);
		System.out.println("Price point: " + clothing[3].price+"$");
		System.out.println("Notes:");
		clothing[3].fit();//calling the instance method from part 1 of the assignment 
 //note to self, add a portion for users to input their own size so we can compare with the information we have
	}
}