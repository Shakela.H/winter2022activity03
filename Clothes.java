public class Clothes{
	public String type;
	public String size;
	public double price;
	
	public void fit(){
		if (this.size.equals("large")){
			System.out.println("This is A size large, might be too big for you.");
		}else if (this.size.equals("medium")){
			System.out.println("It's a perfect fit!");
		}else if (this.size.equals("small")){
			System.out.println("Might be a tight fit on you");
		}else{
			System.out.println("Are you sure you entered the right size?");
		}
	}
}